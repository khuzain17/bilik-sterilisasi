#include <LiquidCrystal_I2C.h>
#include <Adafruit_MLX90614.h>
#include <Wire.h>

const int pin_relay1 = 2; // relay selenoid pintu masuk
const int pin_relay2 = 3; // relay selenoid pintu keluar
const int pin_relay3 = 4; // relay lampu UV

const int pin_led = 5; // led indicator dalam ruangan
const int pin_buzzer = 6; // buzzer indicator dalam ruangan

const int pin_trig1 = 13; // ultrasonic pintu masuk
const int pin_echo1 = 12; // ultrasonic pintu masuk
const int pin_trig2 = 11; // ultrasonic hand sanitizer
const int pin_echo2 = 10; // ultrasonic hand sanitizer
const int pin_trig3 = 9; // ultrasonic dalam ruangan
const int pin_echo3 = 8; // ultrasonic dalam ruangan

Adafruit_MLX90614 mlx = Adafruit_MLX90614(); // sensor suhu no contact

LiquidCrystal_I2C lcd1(0x27, 16, 2);
LiquidCrystal_I2C lcd2(0x26, 16, 2);

long duration;
int hitung_mundur;
int distance;
float suhu_tubuh;
float suhu_tubuh_normal = 38.0;

unsigned long time_now = 0;
int period = 1000;
  
void setup() {
  Serial.begin(9600); 
  
  lcd1.begin(); // untuk lcd1
  lcd2.begin(); // untuk lcd2

  mlx.begin();  

  pinMode(pin_trig1, OUTPUT); 
  pinMode(pin_echo1, INPUT); 
  pinMode(pin_trig2, OUTPUT); 
  pinMode(pin_echo2, INPUT); 
  pinMode(pin_trig3, OUTPUT); 
  pinMode(pin_echo3, INPUT); 

  pinMode(pin_relay1, OUTPUT);
  pinMode(pin_relay2, OUTPUT);
  pinMode(pin_relay3, OUTPUT);

  pinMode(pin_led, OUTPUT);
  pinMode(pin_buzzer, OUTPUT);
}

void display_lcd1 (String upper, String lower) {
  lcd1.setCursor(0,0);
  lcd1.print(upper);
  lcd1.setCursor(0,1);
  lcd1.print(lower);
}

void display_lcd2 (String upper, String lower) {
  lcd2.setCursor(0,0);
  lcd2.print(upper);
  lcd2.setCursor(0,1);
  lcd2.print(lower);
}

int cek_jarak (int pin_trig, int pin_echo) {
  digitalWrite(pin_trig, LOW);
  delayMicroseconds(2);
  
  digitalWrite(pin_trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(pin_trig, LOW);
  
  duration = pulseIn(pin_echo, HIGH);
  distance= duration*0.034/2;
  return distance;

}

float cek_suhu () {
//  Serial.print("*C\tObject = "); Serial.print(mlx.readObjectTempC());
//  Serial.println();
  return (mlx.readObjectTempC());
}

void relay_up (int pin_relay) {
  digitalWrite(pin_relay, HIGH); 
}

void relay_down (int pin_relay) {
  digitalWrite(pin_relay, LOW); 
}


void loop(){
  relay_down(pin_relay2);
  display_lcd1("Dekatkan tangan  ", "pada sensor     ");

  int jarak_input_tangan = cek_jarak(pin_trig1, pin_echo1);
//  Serial.print(jarak_input_tangan);
//  Serial.println();
  delay (500);
  
  if (jarak_input_tangan < 5) { 
    lcd1.clear();
    suhu_tubuh = cek_suhu();
    display_lcd1("Suhu Anda : " , String(suhu_tubuh) + " C");
    delay(2000);
    if (suhu_tubuh > suhu_tubuh_normal) {
        display_lcd1("Mohon maaf, Anda" , "dilarang masuk");
        delay(2000);
        lcd1.clear();
        display_lcd1("Coba lain waktu" , "");
        delay(2000);
        lcd1.clear();
        return 0;
      }
      
    hitung_mundur = 1;
    while (hitung_mundur <= 10) {
      int jarak_tangan_stz = cek_jarak(pin_trig2, pin_echo2);
//      Serial.print("Jarak stz : ");Serial.print(jarak_tangan_stz);
//      Serial.println();

      if (hitung_mundur == 10) {
        return 0;
      }
      
      if (jarak_tangan_stz < 10) {
          display_lcd1("Selamat Datang" , "Silahkan masuk");
          delay(2000);
          break;
      }
      delay(1000);
      hitung_mundur ++;
    }
    
    display_lcd1("Tutup pintu   " , "dengan rapat   ");
    relay_up (pin_relay1);
    delay(2000);

    hitung_mundur = 1;
    while (hitung_mundur <= 10) {
      display_lcd2("Selamat datang" , String(hitung_mundur));
      
      if (hitung_mundur == 10) {
        relay_down (pin_relay1);
        return 0;
      }

      int jarak_badan = cek_jarak(pin_trig3, pin_echo3);

//      Serial.print("Jarak Badan : ");Serial.print(jarak_badan);
//      Serial.println();
      
      if (jarak_badan < 80) { 

        display_lcd1("Silakan menunggu    " , "..............      ");
        
        lcd2.clear();
        display_lcd2("Silakan tutup" , "mata Anda");

        delay (1000);
        digitalWrite(pin_buzzer, HIGH);
        digitalWrite(pin_led, HIGH);
        
        relay_down(pin_relay1);
        delay(3000);
        
        relay_up(pin_relay3);
        delay(10000);
        
        relay_down(pin_relay3);
        digitalWrite(pin_buzzer, LOW);
        digitalWrite(pin_led, LOW);
        display_lcd2("Silakan keluar" , "Terima kasih");
        delay(1000);

        relay_up(pin_relay2);
        delay(5000);
        break;
      }

      delay(1000);
      hitung_mundur ++;
    }
  }

  lcd2.clear();
}
